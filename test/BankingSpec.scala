import org.scalatestplus.play.{OneAppPerSuite, PlaySpec}
import play.api.test.FakeRequest
import play.api.test.Helpers._
import utilities.MessageUtility._

/**
  * Created by law on 03/03/2017.
  */

class BankingSpec extends PlaySpec with OneAppPerSuite {

  val userId = 100

  val balance = "balance"
  val deposit = "deposit"
  val withdraw = "withdraw"
  val logout = "logout"

  val balanceKey = "balance"
  val messageKey = "message"

  var map: Map[String, String] = Map()

  "BankingController" should {
    "return current user account state" in {
      map = contentAsJson(route(app, FakeRequest(GET, s"/$balance/$userId")).get).as[Map[String, String]]
      map(balanceKey).toInt mustBe 0
      map(messageKey) mustBe mBalance

      map = contentAsJson(route(app, FakeRequest(POST, s"/$deposit/$userId/41")).get).as[Map[String, String]]
      map(balanceKey).toInt mustBe 0
      map(messageKey) mustBe mExceededDepositPT

      map = contentAsJson(route(app, FakeRequest(POST, s"/$deposit/$userId/40")).get).as[Map[String, String]]
      map(balanceKey).toInt mustBe 40

      map = contentAsJson(route(app, FakeRequest(POST, s"/$deposit/$userId/40")).get).as[Map[String, String]]
      map = contentAsJson(route(app, FakeRequest(POST, s"/$deposit/$userId/40")).get).as[Map[String, String]]
      map(balanceKey).toInt mustBe 120

      map = contentAsJson(route(app, FakeRequest(POST, s"/$deposit/$userId/40")).get).as[Map[String, String]]
      map(balanceKey).toInt mustBe 120
      map(messageKey) mustBe mExceededDepositPD

      map = contentAsJson(route(app, FakeRequest(POST, s"/$deposit/$userId/10")).get).as[Map[String, String]]
      map(balanceKey).toInt mustBe 130

      map = contentAsJson(route(app, FakeRequest(POST, s"/$deposit/$userId/10")).get).as[Map[String, String]]
      map(balanceKey).toInt mustBe 130
      map(messageKey) mustBe mExceededDepositFrequency

      map = contentAsJson(route(app, FakeRequest(POST, s"/$withdraw/$userId/20")).get).as[Map[String, String]]
      map(balanceKey).toInt mustBe 110

      map = contentAsJson(route(app, FakeRequest(POST, s"/$withdraw/$userId/30")).get).as[Map[String, String]]
      map(balanceKey).toInt mustBe 110
      map(messageKey) mustBe mExceededWithdrawPT

      map = contentAsJson(route(app, FakeRequest(POST, s"/$withdraw/$userId/10")).get).as[Map[String, String]]
      map = contentAsJson(route(app, FakeRequest(POST, s"/$withdraw/$userId/10")).get).as[Map[String, String]]
      map = contentAsJson(route(app, FakeRequest(POST, s"/$withdraw/$userId/10")).get).as[Map[String, String]]
      map(balanceKey).toInt mustBe 90
      map(messageKey) mustBe mExceededWithdrawFrequency

      map = contentAsJson(route(app, FakeRequest(POST, s"/$withdraw/$userId/20")).get).as[Map[String, String]]
      map(balanceKey).toInt mustBe 90
      map(messageKey) mustBe mExceededWithdrawPD

      map = contentAsJson(route(app, FakeRequest(GET, s"/$logout/$userId")).get).as[Map[String, String]]
      map(balanceKey).toInt mustBe 0
      map(messageKey) mustBe mSessionEnded

      map = contentAsJson(route(app, FakeRequest(POST, s"/$deposit/$userId/10")).get).as[Map[String, String]]
      map(balanceKey).toInt mustBe 10

      map = contentAsJson(route(app, FakeRequest(POST, s"/$withdraw/$userId/20")).get).as[Map[String, String]]
      map(balanceKey).toInt mustBe 10
      map(messageKey) mustBe mInsufficientBalance
    }
  }

}
