// The Play plugin
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.5.10")

// code coverage
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.0")
