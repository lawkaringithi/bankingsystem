package akka

import akka.actor.{Actor, ActorLogging, PoisonPill, Props}

/**
  * Created by law on 03/03/2017.
  */

object UserActor {
  def props = Props[UserActor]

  def getActorName(userId: Int) = s"user$userId"

  case class Deposit(amount: Int)

  case class Withdraw(amount: Int)

  case class Balance()

  case class LogOut()

}

class UserActor extends Actor with ActorLogging {

  import UserActor._
  import utilities.MessageUtility._

  /**
    * User state to be stored in database during user log out or termination of their session
    * During initial creation, data should be restored from data store
    */
  var userBalance = 0

  var allowedDepositAmount = 150
  var maxDepositPerTransactionPerTransaction = 40
  var depositFrequencyLeftPerDay = 4

  var allowedWithdrawAmount = 50
  var maxWithdrawPerTransactionPerTransaction = 20
  var withdrawFrequencyLeftPerDay = 3

  override def receive: Receive = {
    case Deposit(amount) => sender() ! formattedResponse(deposit(amount))
    case Withdraw(amount) => sender() ! formattedResponse(withdraw(amount))
    case Balance => sender() ! formattedResponse(mBalance)
    case LogOut => {
      /* Ensure to save current state of the user account */
      restartUserSession()
      sender() ! formattedResponse(mSessionEnded)
      self ! PoisonPill
    }
  }

  /**
    * Attempts to deposit the provided amount to the users account
    *
    * @param amount the amount to be deposited
    * @return String
    */
  def deposit(amount: Int) = {
    if (amount > maxDepositPerTransactionPerTransaction) mExceededDepositPT
    else if (allowedDepositAmount - amount < 0) mExceededDepositPD
    else if (depositFrequencyLeftPerDay == 0) mExceededDepositFrequency
    else {
      depositFrequencyLeftPerDay -= 1
      allowedDepositAmount -= amount
      userBalance += amount
      s"You have deposited USD. $amount."
    }
  }

  /**
    * Attempts to withdraw amount from user account and returns a message to the user
    *
    * @param amount the amount to be withdrawn from the account
    * @return String
    */
  def withdraw(amount: Int) = {
    if (amount > userBalance) mInsufficientBalance
    else if (amount > maxWithdrawPerTransactionPerTransaction) mExceededWithdrawPT
    else if (allowedWithdrawAmount - amount < 0) mExceededWithdrawPD
    else if (withdrawFrequencyLeftPerDay == 0) mExceededWithdrawFrequency
    else {
      withdrawFrequencyLeftPerDay -= 1
      allowedWithdrawAmount -= amount
      userBalance -= amount
      s"You have withdrawn USD. $amount"
    }
  }

  /**
    * Formats the message provided into a map
    *
    * @param message the message response to the user
    * @return Map[String, String]
    */
  def formattedResponse(message: String) = {
    Map("message" -> message, "balance" -> userBalance.toString, "currency" -> "USD")
  }

  /**
    * Reset all variables for user to default
    */
  def restartUserSession(): Unit = {
    userBalance = 0

    allowedDepositAmount = 150
    maxDepositPerTransactionPerTransaction = 40
    depositFrequencyLeftPerDay = 4

    allowedWithdrawAmount = 50
    maxWithdrawPerTransactionPerTransaction = 20
    withdrawFrequencyLeftPerDay = 3
  }

}
