package utilities

/**
  * Created by law on 03/03/2017.
  */

object MessageUtility {

  val mExceededDepositPD = "Exceeded Maximum deposit amount Per Day."
  val mExceededDepositPT = "Exceeded Maximum deposit amount Per Transaction."
  val mExceededDepositFrequency = "Exceeded Maximum deposit frequency Per Day."

  val mExceededWithdrawPD = "Exceeded Maximum withdraw amount Per Day."
  val mExceededWithdrawPT = "Exceeded Maximum withdraw amount Per Transaction."
  val mExceededWithdrawFrequency = "Exceeded Maximum withdraw frequency Per Day."

  val mInsufficientBalance = "Insufficient balance."
  val mBalance = "Balance in USD"
  val mSessionEnded = "Your session has been ended"

}
