import play.api.http.HttpErrorHandler
import play.api.libs.json.Json
import play.api.mvc.Results._
import play.api.mvc._

import scala.concurrent.Future

/**
  * Created by law on 04/03/2017.
  */

class ErrorHandler extends HttpErrorHandler {

  override def onClientError(request: RequestHeader, statusCode: Int, message: String): Future[Result] = {
    Future.successful(
      Status(statusCode)(Json.toJson(Map("message" -> "Sorry, we could not complete your request")))
    )
  }

  override def onServerError(request: RequestHeader, exception: Throwable): Future[Result] = {
    Future.successful(
      InternalServerError(Json.toJson(Map("message" -> "Oh ooh! Something went wrong on our side. Please try again after some time.")))
    )
  }

}
