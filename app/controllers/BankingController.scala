package controllers

import akka.UserActor
import akka.actor.{ActorRef, ActorSystem}
import akka.pattern.ask
import akka.util.Timeout
import com.google.inject.{Inject, Singleton}
import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

/**
  * Created by law on 03/03/2017.
  */

@Singleton
class BankingController @Inject()(actorSystem: ActorSystem)(implicit executionContext: ExecutionContext) extends Controller {

  import BankingController._

  implicit val _actorSystem = actorSystem
  implicit val timeout = Timeout(5 seconds)

  def deposit(userId: Int, amount: Int) = Action.async {
    val future = getActorRef(userId) ? UserActor.Deposit(amount)
    formattedResponse(future).map(response => Ok(response))
  }

  def withdraw(userId: Int, amount: Int) = Action.async {
    val future = getActorRef(userId) ? UserActor.Withdraw(amount)
    formattedResponse(future).map(response => Ok(response))
  }

  def balance(userId: Int) = Action.async {
    val future = getActorRef(userId) ? UserActor.Balance
    formattedResponse(future).map(response => Ok(response))
  }

  def logout(userId: Int) = Action.async {
    val future = getActorRef(userId) ? UserActor.LogOut
    loggedInUser -= userId
    formattedResponse(future).map(response => Ok(response))
  }

}

object BankingController {

  var loggedInUser: mutable.ListBuffer[Int] = ListBuffer()

  /**
    * Creates an ActorRef for the user transacting.
    * If the user is not already logged out, the already existing ActorRef is returned.
    * Otherwise one is created for the user
    *
    * @param userId      the ID of the user transacting
    * @param actorSystem an ActorSystem within the scope used to create the actors
    * @param timeout     a timeout within the scope used to limit amount of time to wait for a response
    * @return ActorRef
    */
  def getActorRef(userId: Int)(implicit actorSystem: ActorSystem, timeout: Timeout): ActorRef = {
    if (loggedInUser.contains(userId)) {
      Await.result(actorSystem.actorSelection(s"akka://application/user/user$userId").resolveOne(), timeout.duration)
    } else {
      val userActor = actorSystem.actorOf(UserActor.props, UserActor.getActorName(userId))
      loggedInUser += userId
      println(userActor.path)
      userActor
    }
  }

  /**
    * Formats the results of the Map[String,String] and returns Future that resolves a Json
    *
    * @param future           the Future that returns the user account current state
    * @param executionContext An ExecutionContext that will be used to resolve the Future
    * @return Future
    */
  def formattedResponse(future: Future[Any])(implicit executionContext: ExecutionContext) = {
    future.mapTo[Map[String, String]].map(response => Json.toJson(response))
  }

}
