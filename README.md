
#Banking System (Account System)
A prototype to demonstrate the development of an account system using **Scala/Play**

##Prerequisites
- [Java 8](https://www.java.com/en/download/mac_download.jsp)
- [Scala](https://www.scala-lang.org/download/)
- [sbt](http://www.scala-sbt.org/download.html)
- [activator](https://www.lightbend.com/activator/download)

##Packaging and running the project
First, you'll need to clone the source;

	$ git clone https://lawkaringithi@bitbucket.org/lawkaringithi/bankingsystem.git

Once that is done, **cd** into the **bankingsystem** directory.

	$ cd bankingsystem

###Dev mode
Running in development does not need packaging. Just run the command;

	$ activator run
	
This starts the server and dispays the output

	$ --- (Running the application, auto-reloading is enabled) ---
	$ [info] p.c.s.NettyServer - Listening for HTTP on /0:0:0:0:0:0:0:0:9000
	$ (Server started, use Ctrl+D to stop and go back to the console...)


###Production Environment
Running in production will require you to use the **dist** command to package the project into a zip file that contains all files required.

While in the project directory, run the command

	$ activator dist

This will generate the zip file in the `target/universal` directory.

You can copy the zip file to a location of your choice or navigate to where it's located to unzip it. We'll go with the latter.

	$ cd target/universal
	$ unzip bankingsystem-1.0-SNAPSHOT.zip
	
Run the app using;
	
	$ bankingsystem-1.0-SNAPSHOT/bin/bankingsystem
	
If it all goes well, you should see some output on the console. Something like this;

	$ [info] play.api.Play - Application started (Prod)
	$ [info] p.c.s.NettyServer - Listening for HTTP on /0:0:0:0:0:0:0:0:9000
	
Which just means your server is now running at [localhost:9000](http://localhost:9000)

##User transactions
Keep in mind that all user transactions are cleared after the user session is terminated (making a **GET** request to **/logout**) or if the web app is restarted.
###Depositing and Withdrawing
Endpoints dealing with deposit and withdrawal are in the format `http://localhost:[port]/[deposit|withdraw]/user_id/amount`

So to deposit $50 to the account with user id 100, make a **POST** request like

	// POST
	http://localhost:9000/deposit/100/50
	
It should return the current balance for the user as well as a message to the user.
	
To withdraw $25 from the same account, make a **POST** request to

	// POST
	http://localhost:9000/withdraw/100/25
	
###Account balance
Account balance is checked by making a **GET** request with the format
`http:localhost:[port]/balance/user_id`

You can check the balance for user with id 100 using

	// GET
	http://localhost:9000/balance/100
	
###Session termination
Terminate the user sessions by making a **GET** request with the format `http://localhost:[port]/logout/user_id`

	// GET
	http://localhost:9000/logout/100
	
##Test coverage
1. UserActor (UserActor.scala) - **100.00%**
2. BankingController (BankingController.scala) - **100.00%**
3. ReverseBankingController (ReverseRoutes.scala) - **0%** - Not covered
4. ReverseBankingController (JavaScriptReverseRoutes.scala) - **0%** - Not covered
5. Routes (Routes.scala) - **77.91%**
6. RoutesPrefix (RoutesPrefix.scala) - **50.00%**
7. MessageUtility - (MessageUtility.scala) - **100.00%**

- Statement coverage:	**77.13 %**
- Branch coverage:	**77.27 %**

##Running your own test coverage
You can also run test coverage by using the commands

	$ sbt clean coverage test
	$ sbt coverageReport
	
This will generate a HTML report in the folder `/target/scala-2.11/scoverage-report`

Open **index.html** to view the coverage results